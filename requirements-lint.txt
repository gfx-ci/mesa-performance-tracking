mypy==1.4.1
ruff==0.4.2
types-PyYAML==6.0.12.10
types-python-dateutil==2.8.19.13
types-requests==2.31.0.1
types-tabulate==0.9.0.2
types-urllib3==1.26.25.13
