#!/usr/bin/env python

# Copyright (C) 2024 Collabora Ltd
# SPDX-License-Identifier: MIT

import asyncio
import logging
import os
import urllib.parse

from contextlib import asynccontextmanager
from dataclasses import dataclass
from functools import cache
from typing import Any, TypeVar, Generic

import aiohttp
from tenacity import (
    AsyncRetrying,
    RetryError,
    retry_if_exception_type,
    stop_after_attempt,
    wait_exponential,
    after_log,
)
from gitlab.v4.objects import ProjectJob
from gitlab.exceptions import GitlabGetError

# Used for Generic type hinting
T = TypeVar("T")


@dataclass
class FetchParams(Generic[T]):
    project_path: str
    session: aiohttp.ClientSession
    db_source: T
    db_points: list[Any]
    params: dict[str, Any]


class PreparedRequest:
    def __init__(self, resource: str, project_path: str, headers: dict[str, str]):
        urlencoded_project_path = urllib.parse.quote(project_path, safe="")
        self.url = (
            f"https://gitlab.freedesktop.org/api/v4/projects/{urlencoded_project_path}/{resource}"
        )
        self.headers = headers | {"PRIVATE-TOKEN": get_auth_token()}


async def inspect_tasks(interval=1):
    while True:
        # Wait for the specified interval (10 seconds by default)
        await asyncio.sleep(interval)

        # Get all tasks
        tasks = asyncio.all_tasks()

        # Ignore this task and loop
        meta_tasks_count = 2

        # Filter out tasks that are done
        pending_tasks = len([task for task in tasks if not task.done()]) - meta_tasks_count

        # Log the number of pending tasks
        logging.info(f"{pending_tasks} pending tasks")


@asynccontextmanager
async def async_inspect(interval_sec=1):
    try:
        inspector_task = asyncio.create_task(inspect_tasks(interval_sec), name="Inspector")
        yield inspector_task
    finally:
        inspector_task.cancel()


def async_retry():
    return AsyncRetrying(
        retry=(
            retry_if_exception_type(GitlabGetError) | retry_if_exception_type(asyncio.TimeoutError)
        ),
        stop=stop_after_attempt(3),
        wait=wait_exponential(multiplier=1, min=1, max=32),
        after=after_log(logging.getLogger(__name__), logging.ERROR),
    )


@cache
def get_auth_token():
    if "GITLAB_TOKEN" in os.environ:
        return os.environ["GITLAB_TOKEN"]
    # try to get it from XDG_CONFIG_HOME/gitlab-token
    if "XDG_CONFIG_HOME" in os.environ:
        xdg_config_home = os.environ["XDG_CONFIG_HOME"]
        token_file = os.path.join(xdg_config_home, "gitlab-token")
        if os.path.exists(token_file):
            with open(token_file, "r") as f:
                return f.read().strip()
    return ""


async def fetch_content(url, session: aiohttp.ClientSession) -> bytes | None:
    async for attempt in AsyncRetrying(
        retry=(
            retry_if_exception_type(GitlabGetError) | retry_if_exception_type(asyncio.TimeoutError)
        ),
        stop=stop_after_attempt(3),
        wait=wait_exponential(multiplier=1, min=1, max=32),
        after=after_log(logging.getLogger(__name__), logging.ERROR),
    ):
        try:
            with attempt:
                # rate limit
                await asyncio.sleep(2)
                # Tiny delay to avoid Error 104 (Connection reset by peer) caused by GIL issues
                # See https://stackoverflow.com/questions/383738/104-connection-reset-by-peer-socket-error-or-when-does-closing-a-socket-resu
                await asyncio.sleep(0.01)
                async with session.get(url) as response:
                    if response.status in (200, 201):
                        return await response.content.read()
                    if response.status in (400, 500, 502, 503, 504):
                        raise GitlabGetError(
                            response_code=response.status,
                            error_message=f"Transient server error {response.status}",
                        )
                    response.raise_for_status()
        except RetryError as e:
            e.reraise()
    return None


async def deferred_artifact_download(
    session: aiohttp.ClientSession,
    project_path: str,
    job_id: int,
    path: str,
) -> bytes | None:
    prepared_request = PreparedRequest("jobs", project_path, {})
    url_path = f"/{job_id}/artifacts/{path}"
    full_path = prepared_request.url + url_path
    content = await fetch_content(full_path, session)
    return content


async def deferred_artifact_download2(
    path: str, job: ProjectJob, session: aiohttp.ClientSession
) -> bytes | None:
    host = job.manager.gitlab.api_url
    url_path = f"{job.manager.path}/{job.encoded_id}/artifacts/{path}"
    full_path = host + url_path
    content = await fetch_content(full_path, session)
    return content


async def get_paginated_results(fp: FetchParams, resource: str, **params):
    page = 1
    prepared_request = PreparedRequest(resource, fp.project_path, {})
    params = fp.params | params
    async for attempt in async_retry():
        try:
            with attempt:
                while True:
                    params.update({"page": page})
                    # See https://stackoverflow.com/questions/383738/104-connection-reset-by-peer-socket-error-or-when-does-closing-a-socket-resu
                    await asyncio.sleep(0.01)
                    async with fp.session.get(
                        prepared_request.url, params=params, headers=prepared_request.headers
                    ) as response:
                        if response.status in (200, 201):
                            data = await response.json()
                            if not data:
                                break
                            last_page = response.headers["x-total-pages"]
                            logging.debug(
                                f"Got {len(data)} items from page {page}/{last_page} for {resource}"
                            )
                            next_page = response.headers["x-next-page"]
                            yield data
                            if not next_page:
                                break
                            page += 1
                            continue
                        if response.status in (400, 500, 502, 503, 504):
                            raise GitlabGetError(
                                response_code=response.status,
                                error_message=f"Transient server error {response.status}",
                            )
                        response.raise_for_status()
        except RetryError as e:
            e.reraise()


async def get_all_results(fp: FetchParams, resource, **params) -> dict | None:
    pr = PreparedRequest(resource, fp.project_path, {})
    params = fp.params | params
    async for attempt in async_retry():
        try:
            with attempt:
                # See https://stackoverflow.com/questions/383738/104-connection-reset-by-peer-socket-error-or-when-does-closing-a-socket-resu
                await asyncio.sleep(0.01)
                async with fp.session.get(pr.url, params=params, headers=pr.headers) as response:
                    if response.status in (200, 201):
                        return await response.json()
                    if response.status in (400, 500, 502, 503, 504):
                        raise GitlabGetError(
                            response_code=response.status,
                            error_message=f"Transient server error {response.status}",
                        )
                    response.raise_for_status()
        except RetryError as e:
            e.reraise()
    return None
