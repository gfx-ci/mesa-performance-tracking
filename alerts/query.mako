import "influxdata/influxdb/schema"

base = from(bucket: "mesa-perf-v2") 
|> range(start: ${start_time}, stop: ${stop_time})
|> filter(fn: (r) => r._field == "commit_sha" or r._field == "frame_time")
|> filter(fn: (r) => r.job_name == "${job_name}" and r.trace_name == "${trace_name}")

commits = base
|> filter(fn: (r) => r._field == "commit_sha")
|> keep(columns: ["_value", "_time"])
|> group(columns: ["_value"])
|> max(column: "_time")
|> group()

previous_scenario = commits
|> top(n: ${sample_size} + ${before_sample_size}, columns: ["_time"])
|> bottom(n: ${before_sample_size}, columns: ["_time"])
|> map(fn: (r) => ({commit_sha: r._value}))

current_scenario = commits
|> top(n: ${sample_size}, columns: ["_time"])
|> map(fn: (r) => ({commit_sha: r._value}))

expanded_data = base
|> schema.fieldsAsCols()
|> group(columns: ["commit_sha"])
|> filter(fn: (r) => r.frame_time > 0)
|> map(fn: (r) => ({r with frame_time: float(v: 1000000000 / r.frame_time)}))
|> median(column: "frame_time", method: "exact_mean")
|> group()

join(tables: {a:current_scenario, b:expanded_data}, on: ["commit_sha"])
|> sort(columns: ["_time"])
|> yield(name: "after")

join(tables: {a:previous_scenario, b:expanded_data}, on: ["commit_sha"])
|> sort(columns: ["_time"])
|> yield(name: "before")

// For guessing the commit range
previous_scenario
|> sort(columns: ["_time"])
|> rename(columns: {"commit_sha": "_value"})
|> median(method: "exact_selector")
|> yield(name: "c1")

// For guessing the commit range
current_scenario
|> sort(columns: ["_time"])
|> rename(columns: {"commit_sha": "_value"})
|> median(method: "exact_selector")
|> yield(name: "c2")
