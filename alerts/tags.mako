import "influxdata/influxdb/schema"

schema.tagValues(bucket: "mesa-perf-v2", tag: "trace_name", start: ${start_time})
|> yield(name: "traces")

schema.tagValues(bucket: "mesa-perf-v2", tag: "job_name", start: ${start_time})
|> yield(name: "jobs")
