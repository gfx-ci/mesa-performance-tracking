import logging
import os
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser, Namespace
from collections import defaultdict
from dataclasses import dataclass, field
from datetime import date, timedelta
from functools import partial
from itertools import chain
from pathlib import Path
from typing import Iterable, Optional

import gitlab
from tabulate import tabulate

from alerts.ministat_debouncer import MinistatDebouncer
from alerts.ministat_description import MinistatDescription
from alerts.ministat_parser import MinistatParser
from alerts.ministat_results import MinistatResults
from alerts.templates import render_template
from common.gl_issue import gl_send_report
from etl.wrappers.influxdb_helper import InfluxdbSource

# Standardize table generation
make_table = partial(tabulate, tablefmt="pipe")

# Who to mention in every report
# TODO: Mention specific users per driver
USER_MENTIONS = ["gallo"]


def get_column_value(table, column="frame_time"):
    for record in table.records:
        yield str(record.values[column])


def normalize(data: list):
    x_min = min(data)
    x_max = max(data)
    delta = x_max - x_min

    return [(x - x_min) / delta for x in data]


def rank(data: Iterable):
    return [sorted(data).index(x) for x in data]


def linear_interpolation(alpha, x0, x1) -> float:
    return alpha * x0 + (1 - alpha) * x1


@dataclass
class MinistatRunner:
    gitlab_url: str = os.environ.get("GITLAB_URL", "https://gitlab.freedesktop.org")
    gitlab_token: str = os.environ.get("GITLAB_TOKEN", "<token unspecified>")
    grafana_host: str = os.environ.get("GRAFANA_HOST", "https://grafana.freedesktop.org")
    dashboard_id: str = os.environ.get("GRAFANA_DASHBOARD_ID", "aH__CPd7z")
    dashboard_name: str = os.environ.get("GRAFANA_DASHBOARD_NAME", "mesa-driver-performance")

    confidence_level: float = 99.5
    # How many commit frame times should be tested from the past to be used as base
    # scenario to test performance changes
    # Greater values reduce standard deviation at the risk of mixing older
    # performance changes plateaus.
    before_sample_size: int = 60
    # After scenario sample size, bigger number will reduce standard deviation, but
    # may pollute the average with before data
    min_after_sample_size: int = 12
    max_after_sample_size: int = 18
    # New traces will have tiny populations in the first analysis. Let's filter
    # out the untimely evaluation and give time to the data to warm up.
    # Please input a number greater or equal to 3. Otherwise, the ministat will
    # fail.
    minimum_sample_size: int = 3
    # Date to mark a version of the report. Changing it to a future value may
    # create a new issue in the target repository.
    report_date: str = "2022-08-01"
    stop_date: str = date.today().isoformat()
    before_days: int = 30
    issue_title: str = "Driver Performance Alerts"
    source = InfluxdbSource("mesa-perf-v2", 4)

    # Influxdb duration string with the period to look for new changes.
    changes_since: str = "-24h"
    # How many changes are enough to trigger alerts?
    minimum_changes: int = 3
    job_traces: dict[str, list[str]] = field(init=False, default_factory=lambda: defaultdict(list))

    def __post_init__(self):
        self.start_date: str = (
            date.fromisoformat(self.stop_date) - timedelta(days=self.before_days)
        ).isoformat()
        self.query_opts = {
            "before_sample_size": str(self.before_sample_size),
            "start_time": self.start_date,
            "stop_time": self.stop_date,
        }

    def __get_days_diff(self):
        try:
            start = date.fromisoformat(self.start_date)
        except ValueError:
            return self.start_date

        delta = date.fromisoformat(self.stop_date) - start
        return str(delta.days)

    def template_query(self, filename, **kwargs):
        query = render_template(filename, **kwargs)
        return self.source.query_api.query(query)

    def detect_new_records(self):
        response = self.template_query(
            "sentinel.mako",
            delta=self.changes_since,
            stop_time=self.query_opts["stop_time"],
        )
        logging.warning("List of excluded traces due to lack of new data:\n\t[")
        for records in response:
            for record in records:
                commit_count = record.get_value()
                job_name = record.values["job_name"]
                trace_name = record.values["trace_name"]
                if commit_count < self.minimum_changes:
                    logging.warning(f"\t\t{job_name} | {trace_name}")
                    continue
                self.job_traces[job_name].append(trace_name)
        logging.warning("\t]")

    def get_all_jobs_and_traces(self) -> tuple[list, list]:
        days_offset = self.__get_days_diff().lstrip("-").rstrip("d")
        response = self.template_query("tags.mako", start_time=f"-{days_offset}d")
        jobs, traces = chain(response)
        traces = [r.get_value() for r in traces.records]
        jobs = [r.get_value() for r in jobs.records]

        return traces, jobs

    def get_normalized_sample_sizes(self) -> dict[str, dict[str, int]]:
        days_offset = self.__get_days_diff().lstrip("-").rstrip("d")
        response = self.template_query(
            "sample_size.mako", start_time=f"-{days_offset}d", stop_time=self.stop_date
        )
        if not response:
            logging.warning(
                "No data found for sample size interpolation. Probably the database is empty."
            )
            return {}

        sample_sizes: dict[str, dict] = defaultdict(dict)
        for record in response[0].records:
            values = record.values
            sample_sizes[values["job_name"]][values["trace_name"]] = record.get_value()

        for traces_sample_size in sample_sizes.values():
            normalized_sample_sizes = normalize(rank(traces_sample_size.values()))
            for idx, trace_name in enumerate(traces_sample_size):
                alpha = normalized_sample_sizes[idx]
                interpolated = linear_interpolation(
                    alpha, self.min_after_sample_size, self.max_after_sample_size
                )
                traces_sample_size[trace_name] = int(interpolated)

        return sample_sizes

    def get_yielded_data(self, response, table):
        for ft in response:
            if ft.columns[0].default_value == table:
                return ft

        raise ValueError(f"Could not find table {table}")

    def get_query_values(self, response) -> tuple[list, list]:
        before = self.get_yielded_data(response, "before")
        after = self.get_yielded_data(response, "after")

        self.commit_range = (
            next(get_column_value(self.get_yielded_data(response, "c1"), column="_value")),
            next(get_column_value(self.get_yielded_data(response, "c2"), column="_value")),
        )
        before = list(get_column_value(before))
        after = list(get_column_value(after))
        return before, after

    def test_scenario(self, job_name, trace_name, sample_size) -> Optional[MinistatResults]:
        response = self.template_query(
            "query.mako",
            job_name=job_name,
            trace_name=trace_name,
            sample_size=sample_size,
            **self.query_opts,
        )

        if len(response) != 4:
            return None

        before, after = self.get_query_values(response)
        if min(len(before), len(after)) < self.minimum_sample_size:
            logging.warning(
                "Before or after population size for %s/%s is lower than %s: %s vs %s. Ignoring it.",  # noqa: E501
                job_name,
                trace_name,
                self.minimum_sample_size,
                len(before),
                len(after),
            )
            return None

        parser = MinistatParser(before, after, self.confidence_level)
        debouncer = MinistatDebouncer()
        if results := debouncer.run(parser, job_name, trace_name):
            return results if results.is_significant() else None

        return None

    def get_gl_project(self, report_target_path):
        gl = gitlab.Gitlab(
            url=self.gitlab_url, private_token=self.gitlab_token, retry_transient_errors=True
        )
        gl.auth()
        return gl.projects.get(report_target_path)

    def generate_grafana_link(self, trace_name, job_name) -> str:
        def to_grafana_epoch(from_date: str) -> int:
            date_obj: date = date.fromisoformat(from_date)
            return int(date_obj.strftime("%s")) * 1000

        from_date = to_grafana_epoch(self.start_date)
        to_date = to_grafana_epoch(self.stop_date)
        return (
            f"{self.grafana_host}/"
            f"d/{self.dashboard_id}/"
            f"{self.dashboard_name}?orgId=1&"
            f"viewPanel=1&"
            f"from={from_date}&to={to_date}&"
            f"var-trace={trace_name}&var-job={job_name}"
        )

    def evaluate_all_jobs(self):
        job_table = partial(
            make_table,
            headers=[
                "\N{Bar Chart} Trace in Grafana",
                "Relative Change (%)",
                "Error",
                "Before Median (FPS)",
                "After Median (FPS)",
                "After Sample Size",
            ],
            floatfmt=".2f",
        )

        self.detect_new_records()
        sample_sizes: dict[str, dict[str, int]] = self.get_normalized_sample_sizes()
        if not sample_sizes:
            return
        # TODO: Use mako to template the report sections
        report = []
        for job, traces in self.job_traces.items():
            job_report = []
            for trace in traces:
                logging.debug(f"Testing job {job}, trace {trace}")
                sample_size = sample_sizes[job].get(trace, self.min_after_sample_size)
                if result := self.test_scenario(
                    job_name=job, trace_name=trace, sample_size=sample_size
                ):
                    link = self.generate_grafana_link(trace, job)
                    decorated_link = f"[{trace}]({link})"
                    trace_report = [
                        decorated_link,
                        *result.relative_data,
                        *result.medians,
                        sample_size,
                    ]
                    job_report.append(trace_report)

            if job_report:
                sorted_job_report = sorted(job_report, key=lambda x: abs(x[1]), reverse=True)
                report.extend((f"## {job}", job_table(sorted_job_report), ""))

        if report:
            # commit_range = (
            # f"Offending commit probable range: {'...'.join(self.commit_range)}"
            # )
            # report.insert(0, commit_range)
            mention_line = "/cc" + " @".join([""] + USER_MENTIONS)
            confidence_level = f"Confidence level: {self.confidence_level:0.2f}%"
            header = [mention_line, "", confidence_level]
            report = header + report

        report = "\n".join(report)
        return report

    def publish_report(self, report, report_target_path: Optional[str] = None):
        if not report:
            print("The report is empty. Not publishing it.")
            return False

        header = f"# Day {self.stop_date}\n"

        description = MinistatDescription(self).render_description()
        print(description)

        if not report_target_path:
            print("The report project path is not set. Can't publish the report.")
            return False

        if gl_send_report(
            self.get_gl_project(report_target_path),
            header + report,
            self.issue_title,
            self.report_date,
            "CI alert",
            description,
        ):
            print("Created an issue")

    def new_report(self, publish=True, report_target_path: Optional[str] = None):
        print(f"Generating report for issue {self.issue_title!a} at {report_target_path!a}")
        print(f"Date range {self.start_date} to {self.stop_date}")
        print(f"Confidence Level {self.confidence_level}%")

        report = self.evaluate_all_jobs()
        if not report:
            print("The report is empty. Not publishing it.")
            return

        print(report)
        if publish:
            self.publish_report(report, report_target_path)


def parse_args() -> Namespace:
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        description=(
            "Wrapper for ministat script."
            " It generates alerts for Mesa CI driver performance using the hypothesis testing."
        ),
        epilog=f"""Example:
        {Path(__file__).name} --confidence-level=99.5""",
    )
    parser.add_argument(
        "-c",
        "--confidence-level",
        type=float,
        default=99.5,
        choices=(80, 90, 95, 98, 99, 99.5),
    )
    parser.add_argument(
        "-rtp",
        "--report-target-path",
        action="store",
        type=str,
        help="Target project path to report Mesa CI driver performance changes, "
        "as a comment on the corresponding gitLab issue in the targeted project",
    )

    return parser.parse_args()


def main():
    args = parse_args()
    logging.basicConfig(level=logging.DEBUG, format="%(message)s")
    mr = MinistatRunner(confidence_level=args.confidence_level)
    report = mr.evaluate_all_jobs()
    print(report)
    mr.publish_report(report, args.report_target_path)


if __name__ == "__main__":
    main()
