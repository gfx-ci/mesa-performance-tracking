#!/bin/bash

set -e

podman_or_docker() {
    if [ -z "$DOCKER_EXEC" ]; then
        if command -v podman &> /dev/null; then
            info "Using podman..."
            DOCKER_EXEC="podman"
        elif command -v docker &> /dev/null; then
            info "Using docker..."
            DOCKER_EXEC="sudo docker"
        else
            error "Please install podman or docker"
        fi
    fi
}

select_compose_exec() {
    if [ -z "$DOCKER_COMPOSE_EXEC" ]; then
        if [ "$DOCKER_EXEC" = "podman" ]; then
            DOCKER_COMPOSE_EXEC="podman compose"
        else
            DOCKER_COMPOSE_EXEC="docker compose"
        fi
    else
        info "Compose exec given by user: ${DOCKER_COMPOSE_EXEC}..."
    fi
}

check_requirements() {
    if ! command -v jq &> /dev/null; then
        error "Please install jq"
    fi

    podman_or_docker

    if [ "$DOCKER_EXEC" = "podman" ]; then
        SOCK_PATH="${XDG_RUNTIME_DIR}/podman/podman.sock"
        export DOCKER_HOST="unix://${SOCK_PATH}"
        export DOCKER_SOCK_VOLUME="${SOCK_PATH}"
    fi

    select_compose_exec
}

argparse() {
    while [ "$#" -gt 0 ]; do
        case "$1" in
            -f|--force)
                FORCE=1
                ;;
            -d|--debug)
                DEBUG=1
                ;;
            -e|--docker-exec)
                DOCKER_EXEC="$2"
                shift
                ;;
            -ce|--docker-compose-exec)
                DOCKER_COMPOSE_EXEC="$2"
                shift
                ;;
            -a|--compose-extra-args)
                COMPOSE_EXTRA_ARGS="$2"
                shift
                ;;
            -p|--project-id)
                PROJECT_ID="$2"
                shift
                ;;
            -r|--registration-token)
                REGISTRATION_TOKEN="$2"
                shift
                ;;
            -g|--gitlab-token)
                GITLAB_TOKEN="$2"
                shift
                ;;
            -h|--help)
                usage
                exit 0
                ;;
            *)
                error "Unknown argument: $1"
                ;;
        esac
        shift
    done

}

usage() {
    cat <<EOF
Usage: $0 [OPTIONS]

Options:
    -f, --force                 Remove existing containers
    -d, --debug                 Run in debug mode
    -e, --docker-exec           Specify the docker executable to use (podman or docker)
    -ce, --docker-compose-exec  Give the docker-compose executable to use
    -a, --compose-extra-args    Extra arguments to pass to docker-compose
    -p, --project-id            Specify the GitLab project ID
    -r, --registration-token    Specify the GitLab runner registration token
    -g, --gitlab-token          Specify the GitLab token
    -h, --help                  Show this help message
EOF
}

# Helper function to get current git remote url
# If it is a git url, transform it into https
# If it is a https url, return it
# If it is not a git repo, return empty string
get_git_remote_url() {
    # Check if the current directory is a git repository
    if ! git rev-parse --is-inside-work-tree > /dev/null 2>&1; then
        error "Not a git repository"
    fi

    # Get the name of the current branch
    branch=$(git rev-parse --abbrev-ref HEAD)

    # Get the name of the remote that this branch is tracking
    remote=$(git config --get branch."${branch}".remote)

    # Check if the remote name is empty
    if [[ -z "$remote" ]]; then
        error "No remote for current branch"
    fi

    # Get the URL of the remote
    url=$(git config --get remote."${remote}".url)

    # Check if the URL is empty
    if [[ -z "$url" ]]; then
        error "No URL for remote '$remote'"
    fi

    # Check if the URL is a git URL and convert it to HTTPS if it is
    if [[ "$url" == git@* ]]; then
        # Convert "git@github.com:user/repo.git" to "https://github.com/user/repo"
        url="${url/:/\/}"  # Replace the first ":" with "/"
        url="https://${url#*@}"  # Remove "git@"
        url="${url%.git}"  # Remove ".git" at the end
    fi

    # Print the URL
    echo "$url"
}

# Get the current project based on the tracked remote
get_gitlab_project_api_url() {
    # Get the HTTPS URL of the remote
    url=$(get_git_remote_url)

    # Check if the URL is empty or not a GitLab URL
    if [[ -z "$url" ]] || [[ "$url" != https://gitlab.* ]]; then
        error "Not a GitLab repository"
    fi

    # Extract the repository path and URL-encode it
    repo_path=$(echo -n "${url#https://gitlab.freedesktop.org/}" | jq -sRr @uri)

    # Transform the URL into a GitLab API URL
    api_url="https://gitlab.freedesktop.org/api/v4/projects/${repo_path}"

    # Return the API URL
    echo "$api_url"
}


warning() {
    # orange color
    echo -e "\033[33m$1\033[0m" >&2
}

info() {
    # green color
    echo -e "\033[32m$1\033[0m" >&2
}

error() {
    # red color
    echo -e "\033[31m$1\033[0m" >&2
    exit 1
}

remove_runners_by_status() (
    info "Removing runners with status $1..."
    curl --silent --request GET \
        --url "https://gitlab.freedesktop.org/api/v4/projects/${PROJECT_ID}/runners?status=${1}" \
        -H "PRIVATE-TOKEN: $GITLAB_TOKEN" | \
    jq .[].id | \
    xargs -I RUNNER_ID curl --silent --request DELETE \
        --url "https://gitlab.freedesktop.org/api/v4/runners/RUNNER_ID" \
        -H "PRIVATE-TOKEN: $GITLAB_TOKEN"
)

validate_token() (
    info "Validating token..."
    # token should start with glrt-
    if [[ ! $1 =~ ^glrt- ]]; then
        error "Invalid token, it should start with 'glrt-', but got '$1'"
    fi
)

register_new_runner() {
    warning "No runner registration token found. Registering a new runner..."
    if [ -z "$PROJECT_ID" ]; then
        error "To register a new runner, please set the PROJECT_ID variable"
    fi
    if [ -z "$GITLAB_TOKEN" ]; then
        error "To register a new runner, please set the GITLAB_TOKEN variable"
    fi
    API_PIDS=()
    remove_runners_by_status "stale" &
    API_PIDS+=($!)
    remove_runners_by_status "never_contacted" &
    API_PIDS+=($!)
    runners_response_json=$(mktemp)
    curl --silent --request POST --url "https://gitlab.freedesktop.org/api/v4/user/runners" \
        -H "PRIVATE-TOKEN: $GITLAB_TOKEN" \
        -d "runner_type=project_type" -d "project_id=${PROJECT_ID}" \
        -d "locked=true" -d "maximum_timeout=3600" -d "access_level=not_protected" \
        -d "description=automated_mpt" -d "tag_list=placeholder-job" > "${runners_response_json}" &
    API_PIDS+=($!)
    wait "${API_PIDS[@]}"
    REGISTRATION_TOKEN=$(jq -r .token "${runners_response_json}")
    if [ "$REGISTRATION_TOKEN" = "null" ]; then
        error "Failed to get registration token for project ID ${PROJECT_ID}: $(jq -r . "${runners_response_json}")"
    fi

    validate_token "$REGISTRATION_TOKEN"
    info "Created new runner with ID: $(jq -r .id "${runners_response_json}")"

    echo "$REGISTRATION_TOKEN"
}

create_docker_external_volumes() {
    info "Creating Docker external volumes..."
    for volume_name in influxdb-data dashboards-data; do
        if ${DOCKER_EXEC} volume ls | grep -q ${volume_name}; then
            info "Volume ${volume_name} already exists"
            continue
        fi
        ${DOCKER_EXEC} volume create ${volume_name}
    done
}

remove_existing_containers() {
    if [ -n "$FORCE" ]; then
        info "Removing existing containers..."
        ${DOCKER_COMPOSE_EXEC} down
        info "Removing monitoring network..."
        ${DOCKER_EXEC} network rm monitoring-network || true
        info "Waiting a little bit for GitLab instance to be ready..."
        sleep 5
    fi
}

start_podman_containers() {
    # Run a rootless API daemon service
    info "Running a rootless API daemon service..."
    if [ ! -S "$SOCK_PATH" ]; then
        info "Creating a new podman socket..."
        ${DOCKER_EXEC} system service --time 0 &
    fi

    if [ -n "$DEBUG" ]; then
        # Run a rootless API daemon service in debug mode
        ${DOCKER_COMPOSE_EXEC} config
    fi

    # Build and run containers
    info "Building and running containers in rootless environment..."
    set -x
    # shellcheck disable=SC2086
    ${DOCKER_COMPOSE_EXEC} up -d ${COMPOSE_EXTRA_ARGS}
    set +x
}

start_docker_containers() {
    info "Building and running containers..."
    set -x
    # shellcheck disable=SC2086
    sudo -E ${DOCKER_COMPOSE_EXEC} up -d ${COMPOSE_EXTRA_ARGS}
    set +x
}

main() {
    argparse "$@"
    check_requirements

    PROJECT_ID=${PROJECT_ID:-$(get_gitlab_project_api_url | xargs curl --silent | jq .id)}

    if [ -z "$PROJECT_ID" ]; then
        error "Cannot find a project id."
    fi

    if [ -z "$REGISTRATION_TOKEN" ]; then
        REGISTRATION_TOKEN=$(register_new_runner)
    else
        validate_token "$REGISTRATION_TOKEN"
    fi
    export REGISTRATION_TOKEN
    info "Using gitlab runner registration token: $REGISTRATION_TOKEN"
    info "Using ${DOCKER_COMPOSE_EXEC:?}..."

    create_docker_external_volumes
    remove_existing_containers

    info "Removing existing config.toml..."
    rm -f gitlab-runner/config.toml
    rm -f gitlab-runner-config/.runner_system_id

    PODMAN_COMPOSE_WARNING_LOGS=false
    export PODMAN_COMPOSE_WARNING_LOGS

    if [ "$DOCKER_EXEC" = "podman" ]; then
        start_podman_containers
    else
        start_docker_containers
    fi
}

main "$@"
