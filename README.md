<div align="center">

# Mesa performance tracking

This project provides powerful insights into the performance of Mesa drivers and Mesa's CI,
all displayed beautifully through Grafana dashboards.

Whether you're monitoring pipeline quality,
driver performance, or comparing drivers,
we've got you covered!

![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffffff)
![Grafana](https://img.shields.io/badge/grafana-%23F46800.svg?style=for-the-badge&logo=grafana&logoColor=white)

</div>

![Mesa CI Quality Stats](img/overall_mesa.gif "Mesa CI quality stats screencast")*Mesa CI quality stats screencast*

# Table of Contents

- [Overview](#overview)
  - [Details](#details)
  - [System architecture](#system-architecture)
- [Automation](#automation)
  - [Extra scripts](#extra-scripts)
    - [External scripts](#external-scripts)
- [Testing](#testing)
  - [Requirements](#requirements)
  - [Running tests locally](#running-tests-locally)
    - [Recommended](#recommended)
    - [Manual steps](#manual-steps)
- [Dashboard deployment](#dashboard-deployment)
  - [Updating dashboards](#updating-dashboards)
  - [Creating dashboards](#creating-dashboards)

## Overview

In a nutshell, Mesa Performance Tracking produces four central dashboards:

- [Mesa CI Quality - False Positives](https://ci-stats-grafana.freedesktop.org/d/Ae_TLIwVk/mesa-ci-quality-false-positives)
  - shows various metrics related to false positives in the CI pipeline, including:
    - failed merges due to consecutive pipeline failures
    - deqp/skqp/piglit flaky stats
    - LAVA jobs retries

- [From Marge to Merge](https://ci-stats-grafana.freedesktop.org/d/n7guL8ySz/from-marge-to-merge)
  - provides the time from when a developer assigns to marge-bot to the time it was merged and also dissects the pipeline duration

- [Mesa Performance Driver](https://ci-stats-grafana.freedesktop.org/d/aH__CPd7z/mesa-performance-driver)
  - shows how mesa drivers perform via piglit traces replay profiling mode

- [Performance Driver Comparison](https://ci-stats-grafana.freedesktop.org/d/Ge8lBATVk/performance-driver-comparison)
  - compares alternative drivers performance; e.g., `a618` on `freedreno` vs. `a618` on `zink+turnip`

### Details

1. **GitLab API**: The root source of all data. Python scripts in the [etl/](etl/) directory scrape job data from GitLab.
   - We use [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/) to interact with the GitLab API.
   - And also, we have an asynchronous version of the script in the [etl/basic_stats.py](etl/basic_stats.py) file to deal with the mass of the data.
1. **Extract and Transform**: Some of the scraped data undergoes analysis to reduce size, making it more manageable for querying.
1. **InfluxDB**: The data is fed into InfluxDB (a time-series database) in a reduced form, allowing quick and efficient querying.
1. **Grafana**: Grafana sources data from InfluxDB, using Flux queries (a SQL-like language) to power each panel on the dashboard.

### System architecture

```mermaid
graph LR
   subgraph GitLab API
      AsyncWrapper[/Custom async wrapper\]
      PythonGL[/python-gitlab\]
   end

   subgraph Database
      InfluxDB[(InfluxDB)]
   end

   subgraph ETL
      BasicStats[etl/basic_stats.py] -.-> AsyncWrapper
      BasicStats --> |Send raw Data| InfluxDB
      AllETLScripts[etl/**/*.py] -.-> PythonGL
      AllETLScripts --> |Send transformed Data| InfluxDB
   end

   subgraph Extra Scripts
      MinistatRunner[ministat_runner.py]
      CIStatus[mesa_ci_status.py]
      TriggerJobs[trigger_jobs.py]
      PendingMonitor[mesa_ci_pending_monitor.py]
      MinistatRunner -.-> |Update Perf Issue| PythonGL
      InfluxDB --> |Reuses data from| MinistatRunner
   end

   subgraph External Scripts
      GanttScript[mesa/mesa: mesa_ci_gantt.py]
   end

   ScheduledPipelines[GitLab CI/CD]
   subgraph Scheduled Pipelines
      ScheduledPipelines --> UpdateDashboards>Update dashboards]
      ScheduledPipelines --> PerfAlerts>Send driver performance alerts]
      ScheduledPipelines --> PerfJobs>Trigger performance jobs]
      ScheduledPipelines --> Status>Pipelines status report to GitLab Issue]
   end

   PerfJobs ---> |For mesa/mesa merged pipelines|TriggerJobs
   Status --> |Former data mining| CIStatus -.-> |Update CI daily issues| PythonGL

   UpdateDashboards --> |Deploy dashboards| Exporter
   UpdateDashboards --> |Mine common GitLab data| BasicStats
   UpdateDashboards --> |Mine specific GitLab data| AllETLScripts

   subgraph Dashboards
      Exporter[dashboard_exporter/exporter.py] --> |Push dashboard model updates| Grafana((Grafana))
      InfluxDB --> |Gets time-series data| Grafana
      Exporter --> |Get raw Grafana JSON model| JSONModels[dashboards/*.json]
      Exporter --> |Get the glue for JSON Models| Metadata[dashboards/metadata.yml]
   end

   PerfAlerts --> |Analyze driver perf data| MinistatRunner
   TriggerJobs -.-> |Play -performance jobs| PythonGL

   CollaboraServer[Collabora Server] --> |Detects pending jobs| PendingMonitor -.-> |Update CI pending issues| PythonGL
   CollaboraServer ---> |From marge warning to Gantt reply| GanttScript -.-> |Update MR comments| PythonGL


   linkStyle 0,1,2,3,5,6,13,14,15,16,17,18,19 stroke: red
   linkStyle 4,5,7,20 stroke: blue
   linkStyle 9,11,12 stroke: green
   linkStyle 8,10,21 stroke: orange
   linkStyle 22,23,24,25 stroke: purple

   style Grafana stroke:#444,stroke-width:2px,rx: 5,ry: 5
   style InfluxDB stroke:#444,stroke-width:2px,rx: 5,ry: 5
   style ScheduledPipelines stroke:#444,stroke-width:2px,rx: 5,ry: 5

```

*System architecture diagram*

### Automation

We have several GitLab CI/CD [scheduled pipelines](https://gitlab.freedesktop.org/gfx-ci/mesa-performance-tracking/-/pipeline_schedules) that run periodically to update the data and the
dashboards.
Also, we have some scripts to trigger jobs and send alerts.

![scheduled pipelines](img/schedules.png "Scheduled pipelines")*CI/CD scheduled pipelines screenshot*

| Current pipelines | Description | Scripts |
| --- |  --- | --- |
| Hourly dashboard update | Updates the [dashboards](https://ci-stats-grafana.freedesktop.org/dashboards/) in Grafana | [exporter.py](dashboard_exporter/exporter.py) |
| Mesa CI driver performance check alerts | Checks daily for driver performance regressions in InfluxDB and reports them as comments to the [Driver Performance Alerts](https://gitlab.freedesktop.org/mesa/mesa/-/issues/7144) issue. | [ministat_runner.py](alerts/ministat_runner.py) |
| Mesa CI Post-merge performance jobs | Triggers performance jobs following successful merge requests. Runs twice per hour. | [trigger_jobs.py](trigger_jobs.py) |
| Run Daily CI Report | Reports daily stats in [CI daily issues](https://gitlab.freedesktop.org/mesa/mesa/-/issues/?sort=created_date&state=opened&label_name%5B%5D=CI%20daily&first_page_size=100) | [mesa_ci_status.py](mesa-ci-status/mesa_ci_status.py) |
|  |  Script to check for jobs with large pending times and report them as: [pending job alerts](https://gitlab.freedesktop.org/mesa/mesa/-/issues/?sort=created_date&state=opened&label_name%5B%5D=CI%20alert&first_page_size=100) | [mesa_ci_pending_monitor.py](mesa-ci-status/mesa_ci_pending_monitor.py) |

## Testing

### Requirements

- `docker-compose` 3.7+ or `podman-compose`
- `podman` 4+ or `docker` 18.06.0+
- `git`
- `python` 3.8+
- `python3-pip`
- `python3-dev`
- `gcc`
- `libkrb5-dev` for `grafana-dashboard-builder` python requirement
- `musl-dev` if using Alpine OS

### Running tests locally

#### Recommended

- You will need a GitLab [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with API scope.
- Fork the project [gfx-ci/mesa-performance-tracking](https://gitlab.freedesktop.org/gfx-ci/mesa-performance-tracking) and get the project ID.
  - One way to get the project ID is by going to your fork via browser and clicking on the ellipsis menu (⋮) in the top right corner, just after the fork button.
   ![project id](img/pid_loc.png "Project ID location")

- Add two new variables to your fork's `CI/CD Settings`:

   | KEY   | VALUE    |
   |-------|----------|
   | `GITLAB_TOKEN`| __your personal access token__  |
   | `GRAFANA_URL`| http://admin:admin@grafana:3000  |

- Optional variables that can also be added to `CI/CD Settings`:


   | KEY   | VALUE    | DESCRIPTION      |
   |-------|----------|------------------|
   | `RUN_THIS_STAGE_ONLY`| send-reports  | To run only report generation and publishing  |
   | `RUN_THIS_STAGE_ONLY`| trigger-jobs  | To only run trigger-jobs job                  |
   | `RUN_THIS_STAGE_ONLY`| dashboards    | To update dashboards and monitoring data      |
   | `RUN_THIS_STAGE_ONLY`| test          | To run only lint and test jobs                |

- Run the `bootstrap_local_containers.sh` script to create containers for a gitlab-runner and your local instances of grafana and influxDB.
   ```sh
   ./bootstrap_local_containers.sh \
      --gitlab-token "???" \
      --project-id "???" \
      --docker-exec podman
   ```
   > Note: if using podman check that the service is started 'systemctl --user status podman'

  - Or export the variables separately

     ```sh
     export GITLAB_TOKEN="???" # See above
     export PROJECT_ID="???"  # See above
     export DOCKER_EXEC=podman  # or "docker"
     ```

- Run a pipeline via Gitlab in your fork

- Access the grafana dashboard at: <http://localhost:3000>
  - Default login: `admin`, password: `admin`

- Access the influxdb data sources at: <http://localhost:8086>
  - Default login: `admin`, password: `adminadmin`
   > Note default credentials can be changed in [docker-compose.yaml](docker-compose.yaml)


#### Manual steps

A Docker compose definition is provided so contributors can test their changes locally before submitting them for review.

Steps:

1. Install python requirements with `pip install -r requirements.txt`
1. Create a personal access token for your [user](https://gitlab.freedesktop.org/-/profile/personal_access_tokens) with the API scope and set it as a masked CI/CD variable in your forked repo with the name `GITLAB_TOKEN`
1. Disable shared runners in the user's fork repo
1. Set `GRAFANA_URL=http://admin:admin@grafana:3000` as a CI/CD variable in your forked repo
1. **For Podman rootless users** To run the locally rootless version, one should run a rootless API daemon service via: `podman system service --time 0 &`. **If you opt to run in docker+sudo, please remember to add it accordingly from the rest of this document**.
1. Create a docker external volume to persist data without attaching them to the docker-compose with:

   ```bash
   # change `podman` to `sudo docker` if using docker
   podman volume create influxdb-data  # InfluxDB
   podman volume create dashboards-data  # Grafana
   ```

1. Disable shared runners in the user's fork repo

1. Build and run the containers containing Grafana, InfluxDB and gitlab-runner, specifying the runner registration token for your fork. To accomplish that, run `docker-compose` pointing to the rootless API and overriding the rootless options.

   ```bash
   RUNNER_REGISTRATION_TOKEN=[enter your token here] \
      podman-compose \
         -f docker-compose.yaml \
         up
   ```

1. Or, with docker+sudo

   ```bash
   RUNNER_REGISTRATION_TOKEN=[enter your token here] \
      sudo -E docker-compose \
         -f docker-compose.yaml \
         up
   ```

1. Check that you can log into the Grafana service by going to <http://localhost:3000> and using the [default admin account](https://grafana.com/docs/grafana/latest/administration/configuration/#admin_user).
1. Check that you can log into the InfluxDB service by going to <http://localhost:8086> and using the username and password from `docker-compose.yaml` file.
1. If you want to run scheduled pipelines, you can set the RUN_THIS_STAGE_ONLY CI/CD variable for the following use cases:
    - RUN_THIS_STAGE_ONLY=send-reports  # To run only report generation and publishing
    - RUN_THIS_STAGE_ONLY=trigger-jobs  # To only run trigger-jobs job
    - RUN_THIS_STAGE_ONLY=dashboards    # To update dashboards and monitor data
    - RUN_THIS_STAGE_ONLY=test          # To only run lint and test jobs
1. To set arguments for each script, you can set the below CI/CD variable
    - MESA_CI_STATUS_ARGS - See mesa-ci-status/mesa_ci_status.py for the argument list
    - PERFORMANCE_ALERT_ARGS - See alerts/ministat_runner.py for the argument list
1. Execute a pipeline in your fork
1. After the pipeline finishes, test your changes in <http://localhost:3000>

## Dashboard deployment

We are deprecating `grafana-dashboard-builder` in favor of `dashboard-exporter`.
It supports creating and editing dashboards as code.

Currently, only the "Mesa Driver Performance" dashboard is using the new tool.
Eventually, the other ones will migrate to the new one as well.

### Updating dashboards

1. Update dashboard title, datasource name, dashboard uid etc. by editing
`dashboards/metadata.yml`
2. Update the JSON model by overwriting an existing JSON file inside
`./dashboards` and ensure that it is listed in `metadata.yml`

### Creating dashboards

1. Open any dashboard in fd.o [Grafana instance](https://ci-stats-grafana.freedesktop.org).
1. Copy the JSON model to a file and put it in the `./dashboards` folder
   - You have to be an admin or the dashboard's author
   - You can find it by appending: `&editview=dashboard_json` to the dashboard URL
1. Update `dashboards/metadata.yml` the same way as the other dashboards
   - Example:

   ```yaml
   dashboard_title:
    json_model: model.json

    # overrides are optional
    # it is more used for migrating data easily
    overrides:
      uid: myid
      datasource:
        uid: mydb
   ```

1. Run the dashboard exporter script:

   ```bash
   PYTHONPATH=. dashboard_exporter/exporter.py create_or_update dashboards/metadata.yml
   ```

1. Check if the new dashboard appears in the dashboards General folder, which
is available at `/dashboards` endpoint
