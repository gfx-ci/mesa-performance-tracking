#!/usr/bin/python3

# Copyright (c) 2020 Collabora Ltd
# SPDX-License-Identifier: MIT

import datetime
import os

import gitlab
import influxdb_client
from dateutil import parser
from dateutil.relativedelta import relativedelta

GITLAB_URL = os.environ.get("GITLAB_URL", "https://gitlab.freedesktop.org")
GITLAB_PROJECT_PATH = os.environ.get("GITLAB_PROJECT_PATH", "mesa/mesa")
GITLAB_TOKEN = os.environ.get("GITLAB_TOKEN", "<token unspecified>")
INFLUX_URL = os.environ.get("INFLUX_URL", "http://influxdb:8086")
INFLUX_ORG = os.environ.get("INFLUX_ORG", "freedesktop")
STATS_INFLUX_BUCKET = os.environ.get("STATS_INFLUX_BUCKET", "marge-stats")
STATS_INFLUX_TOKEN = os.environ.get("STATS_INFLUX_TOKEN", "influx-testing-token")


def get_last_write(influx):
    query = """from(bucket:"{}")
               |> range(start: -1y)
               |> filter(fn: (r) => r["_measurement"] == "job_failure")
               |> group()
               |> sort(columns:["_time"])
               |> last()"""
    query = query.format(STATS_INFLUX_BUCKET)
    result = influx.query(query, org=INFLUX_ORG)
    if len(result) == 0:
        return datetime.datetime.now(datetime.timezone.utc) - relativedelta(weeks=1)
    last_write_time = result[0].records[0].get_time()
    seconds_precision = relativedelta(seconds=1)
    return last_write_time + seconds_precision


def write_pipeline_failure(influx, pipeline_time, pipeline_id):
    print(f"Writing pipeline {pipeline_time}")

    p = influxdb_client.Point("pipeline_failure")
    p = p.tag("pipeline", pipeline_id)
    p = p.field("value", 1)
    p = p.time(pipeline_time)
    influx.write(bucket=STATS_INFLUX_BUCKET, org=INFLUX_ORG, record=p)


def write_job_failure(influx, job_time, job_name, job_stage, pipeline_id):
    print(f"Writing job {job_time}")

    p = influxdb_client.Point("job_failure")
    p = p.tag("job_name", job_name)
    p = p.tag("job_stage", job_stage)
    p = p.tag("pipeline", pipeline_id)
    p = p.field("value", 1)
    p = p.time(job_time)
    influx.write(bucket=STATS_INFLUX_BUCKET, org=INFLUX_ORG, record=p)


if __name__ == "__main__":
    gl = gitlab.Gitlab(url=GITLAB_URL, private_token=GITLAB_TOKEN, retry_transient_errors=True)
    project = gl.projects.get(GITLAB_PROJECT_PATH)

    influx_client = influxdb_client.InfluxDBClient(
        url=INFLUX_URL, token=STATS_INFLUX_TOKEN, org=INFLUX_ORG
    )

    since = get_last_write(influx_client.query_api())

    for pipeline in project.pipelines.list(
        iterator=True,
        per_page=50,
        sort="asc",
        updated_after=since.isoformat(),
        username="marge-bot",
    ):
        updated_at = parser.parse(pipeline.updated_at)
        print("Processing pipeline %d last updated at %s" % (pipeline.id, pipeline.updated_at))
        pipeline_failed = False
        with influx_client.write_api() as write_api:
            for job in pipeline.jobs.list(iterator=True, per_page=150):
                job_name = job.name
                job_name_split = job_name.split(" ")
                if "/" in job_name_split[-1]:
                    job_name = " ".join(job_name_split[:-1])
                if job.status == "failed":
                    pipeline_failed = True
                    write_job_failure(write_api, updated_at, job_name, job.stage, pipeline.id)
            if pipeline_failed:
                write_pipeline_failure(write_api, updated_at, pipeline.id)
            write_api.flush()
