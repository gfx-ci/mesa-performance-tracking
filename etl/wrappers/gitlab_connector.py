#!/usr/bin/python3

# Copyright © 2021 Collabora Ltd.
# SPDX-License-Identifier: MIT

import bz2
import json
import logging
import time
from dataclasses import dataclass
from datetime import datetime
from functools import singledispatchmethod
from os import environ
from typing import Any, ClassVar, Generator, Optional

import requests
from dateutil import parser as date_parser
from gitlab.base import RESTObject
from gitlab.client import Gitlab
from gitlab.exceptions import GitlabGetError
from gitlab.v4.objects import (
    MergeRequest,
    Project,
    ProjectJob,
    ProjectMergeRequest,
    ProjectPipeline,
    ProjectPipelineJob,
)
from requests.exceptions import ChunkedEncodingError


@dataclass
class GitlabConnector:
    _default_pipeline_args: ClassVar[dict[str, Any]] = {
        "iterator": True,
        # The default value of this parameter is 20, let's increase it to reduce
        # the number of API calls, balancing between bandwidth and TTL.
        "per_page": 160,
    }
    _project_path: str = environ.get("GITLAB_PROJECT_PATH", "mesa/mesa")
    _url: str = environ.get("GITLAB_URL", "https://gitlab.freedesktop.org")
    _gitlab_token: str = environ.get("GITLAB_TOKEN", "<token unspecified>")
    _username: str = environ.get("GITLAB_USER", "marge-bot")
    pipeline_wait_time_sec: int = 2

    def __post_init__(self) -> None:
        self.__gl: Gitlab = Gitlab(
            url=self._url,
            private_token=self._gitlab_token,
            retry_transient_errors=True,
        )
        self._project: Project = self.__gl.projects.get(self._project_path)

    @property
    def project(self) -> Project:
        return self._project

    @property
    def project_path(self) -> str:
        return self._project_path

    @singledispatchmethod
    def gitlab_timeformat(self, timestamp: str) -> str:
        return timestamp

    @gitlab_timeformat.register
    def _(self, timestamp: datetime) -> str:
        return timestamp.isoformat()

    def get_merged_at_from_pipeline(self, pipeline: ProjectPipeline) -> Optional[str]:
        try:
            mr_id = pipeline.ref.split("/")[-2]
            return self.project.mergerequests.get(mr_id).merged_at
        except (AttributeError, IndexError, GitlabGetError) as e:
            logging.error(f"Exception while getting merged_at for pipeline {pipeline.id}: {e}")
            return None

    def pipelines_from_branch(
        self,
        remote_branch: str = "main",
        source: str = "merge_request_event",
        **kwargs,
    ) -> list[ProjectPipeline]:
        """
        Retrieve pipelines from a specific branch of the GitLab project.

        Args:
            remote_branch (str, optional): The branch to retrieve pipelines
            from. Defaults to "main".
            **kwargs: Additional arguments to pass to the GitLab API.

        Returns:
            list[ProjectPipelineJob]: A list of project pipeline jobs.
        """
        list_args = {
            **self._default_pipeline_args,
            "ref": remote_branch,
        } | kwargs
        return list(self._project.pipelines.list(**list_args))  # type: ignore

    def get_proj_mr_merged_after(
        self, merged_after: datetime, mr: MergeRequest | RESTObject
    ) -> Optional[ProjectMergeRequest | RESTObject]:
        """
        Retrieve a project merge request object if it was merged after a
        specific date.

        Args:
            merged_after (datetime): Only return MRs merged after the specified
            date.
            mr (MergeRequest): A merge request object.

        Returns:
            Optional[ProjectMergeRequest]: A project merge request object.
        """
        if datetime.fromisoformat(mr.merged_at.rstrip("Z")) <= merged_after.replace(tzinfo=None):
            return None
        # items from list only comes in partial form, using get() to get all info.
        return self._project.mergerequests.get(mr.iid)

    def mrs_in_main_merged_after(
        self, merged_after: datetime, **kwargs
    ) -> Generator[ProjectMergeRequest | RESTObject, None, None]:
        """
        Retrieve merge requests targeting the "main" branch.

        Args:
            merged_after (datetime): Only return MRs merged after the specified
            date.
            **kwargs: Additional arguments to pass to the GitLab API.

        Returns:
            Generator[ProjectMergeRequest, None, None]: A generator of project MRs.
        """
        list_args = {
            **self._default_pipeline_args,
            "updated_after": merged_after.isoformat(),
            "target_branch": "main",
            "state": "merged",
            **kwargs,
        }
        for mr in self._project.mergerequests.list(**list_args):
            # MRs can be updated after a merge, so we need to guarantee that
            # we are only looking at the merged date, unfortunately, Gitlab does
            # not support that.
            if merged_after_mr := self.get_proj_mr_merged_after(merged_after, mr):
                yield merged_after_mr

    def main_pipelines_from_mrs(
        self, merged_after: datetime, **kwargs
    ) -> Generator[ProjectPipeline, None, None]:
        """
        Retrieve pipelines from merge requests targeting the "main" branch which
        were merged after a specific date.

        Args:
            merged_after (datetime): Only return end pipelines merged after the
            specified date.
            **kwargs: Additional arguments to pass to the GitLab API.

        Returns:
            Generator[ProjectPipeline, None, None]: A generator of project pipelines.
        """
        list_args = {
            **self._default_pipeline_args,
            "merged_after": merged_after,
            "target_branch": "main",
            "state": "merged",
            **kwargs,
        }
        for mr in self.mrs_in_main_merged_after(**list_args):
            yield self._project.pipelines.get(mr.pipeline["id"])

    def pipeline_list(
        self, updated_after: datetime, **kwargs
    ) -> Generator[ProjectPipeline, None, None]:
        list_args = {
            **self._default_pipeline_args,
            "sort": "asc",
            "updated_after": updated_after.isoformat(),
            "username": self._username,
        } | kwargs
        yield from self._project.pipelines.list(**list_args)  # type: ignore

    def success_marge_pipelines_merged_after(
        self, merged_after: datetime, **kwargs
    ) -> Generator[ProjectPipeline, None, None]:
        """
        Retrieve pipelines merged after a specific date.

        Args:
            merged_after (datetime): Only return end pipelines merged after the
            specified date.
            **kwargs: Additional arguments to pass to the GitLab API.

        Returns:
            Generator[ProjectPipeline, None, None]: A generator
            of tuples containing the pipeline and the date it was merged set at
            merged_at attribute.
        """
        logging.info(f"Getting Marge pipelines merged after {merged_after}")
        list_args = {
            **self._default_pipeline_args,
            "updated_after": merged_after,
            "username": "marge-bot",
            "status": "success",
            "source": "merge_request_event",
        } | kwargs
        for pipeline in self.pipeline_list(**list_args):
            merged_at: Optional[str] = self.get_merged_at_from_pipeline(pipeline)
            if merged_at is None:
                logging.warning(f"Could not get merged_at for pipeline {pipeline.id}. Skipping.")
                continue
            pipeline.merged_at = date_parser.parse(merged_at)
            yield pipeline

    def wait_for_pipeline(self, sha):
        while True:
            if pipelines := self._project.pipelines.list(sha=sha):
                return pipelines[0] if isinstance(pipelines, list) else pipelines.next()
            time.sleep(self.pipeline_wait_time_sec)

    def get_pipeline(self, pipeline_id: int) -> ProjectPipeline:
        return self._project.pipelines.get(pipeline_id)

    @staticmethod
    def get_artifact_via_requests(job: ProjectPipelineJob, path: str) -> Optional[bytes]:
        """
        Get artifact from a job using requests instead of the Gitlab API.  This
        is useful when you only have a ProjectPipelineJob object and don't want
        to fetch ProjectJob via jobs.get API call, saving up one request per
        job.

        Args:
            job (ProjectPipelineJob): A project pipeline job object.
            path (str): The path to the artifact.

        Returns:
            Optional[bytes]: The artifact content. None if the artifact doesn't
            exist.
        """
        if job.pipeline["status"] == "running":
            logging.warning(f'Pipeline {job.pipeline["id"]} is still running')

        if job.status != "success":
            logging.warning(
                f"Job {job.web_url} has status {job.status}. Skipping downloading artifacts."
            )
            return None

        url = f"{job.web_url}/artifacts/raw/{path}"
        logging.debug(f"Requesting results from: {url}")
        r = requests.get(url, timeout=10)

        if r.status_code != 200:
            logging.debug(f"Failed to download ({r.status_code}):\n\t{url}")
            return None

        return r.content

    @staticmethod
    def get_results(job: ProjectPipelineJob) -> dict[str, Any]:
        artifact_data = GitlabConnector.get_artifact_via_requests(job, "results/results.json.bz2")
        return json.loads(bz2.decompress(artifact_data)) if artifact_data else {}

    @staticmethod
    def get_structured_log(job: ProjectPipelineJob) -> dict[str, Any]:
        structured_log_file_path = "results/lava_job_detail.json"
        lava_json = GitlabConnector.get_artifact_via_requests(job, structured_log_file_path)
        if not lava_json:
            return {}
        try:
            structured_log = json.loads(lava_json)
        except json.JSONDecodeError as e:
            print("Error:", e)
            return {}

        return structured_log

    @staticmethod
    def get_artifact(job: ProjectJob, path: str):
        try:
            if artifact := job.artifact(path):
                if not isinstance(artifact, bytes):
                    artifact = bytes(artifact)

                return artifact.decode("UTF-8")
        except GitlabGetError as e:
            # File doesn't exist, ignore
            if e.response_code != 404:
                raise
            logging.debug(f"Artifact {path} doesn't exist for job {job.web_url}")
        except ChunkedEncodingError as e:
            logging.error(f"Failed to download artifact {path} for job {job.web_url}: {e}")
            logging.error(
                "Probably the file does not exist and Gitlab "
                " is responding with wrong code, "
                "https://github.com/python-gitlab/python-gitlab/issues/1186"
            )
        return None
