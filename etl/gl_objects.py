#!/usr/bin/env python

# Copyright (C) 2024 Collabora Ltd
# SPDX-License-Identifier: MIT


from influxdb_client import Point


from typing import Literal, TypedDict


class DBMeasurementsDict(TypedDict):
    merge_request: str | list[str]
    pipeline: str | list[str]
    job: str | list[str]
    mr_notes: str | list[str]
    sl: str | list[str]
    sl_jobs: str | list[str]
    sl_phases: str | list[str]
    utest: str | list[str]
    failed_job: str | list[str]


DB_MEASUREMENTS_KEYS = Literal[
    "merge_request",
    "pipeline",
    "job",
    "mr_notes",
    "sl",
    "sl_jobs",
    "sl_phases",
    "utest",
    "failed_job",
]

DB_MEASUREMENTS: DBMeasurementsDict = {
    "merge_request": "gl-mr-stats",
    "pipeline": "gl-pipeline-stats",
    "job": "gl-job-stats",
    "mr_notes": "gl-mr-notes",
    "sl": "gl-sl-stats",
    "sl_jobs": "gl-sl-jobs",
    "sl_phases": "gl-sl-phases",
    "utest": "gl-utest-stats",
    "failed_job": "gl-failed-jobs",
}


TAG_CLASS: DBMeasurementsDict = {
    "job": [
        "allow_failure",
        "failure_reason",
        "name",
        "retried",
        "project_path",
        "stage",
        "status",
        "tag_list",
    ],
    "pipeline": ["status", "project_path", "source"],
    "merge_request": ["state", "project_path", "target_branch"],
    "mr_notes": ["system", "project_path"],
    "sl": [
        "dut_job_type",
        "farm",
        "job_combined_fail_reason",
        "job_combined_status",
        "dut_attempt_counter",
    ],
    "sl_jobs": [
        "dut_job_type",
        "status",
        "dut_state",
        "job_attempt",
    ],
    "sl_phases": ["job_attempt"],
    "utest": ["job_name", "stage", "reason", "test_suite"],
    "failed_job": ["job_name", "category", "tag", "stage"],
}

FIELD_CLASS: DBMeasurementsDict = {
    "job": [
        "duration",
        "finished_at",
        "full_name",
        "id",
        "pipeline_id",
        "queued_duration",
        "sha",
        "started_at",
        "username",
        "web_url",
    ],
    "pipeline": [
        "created_at",
        "duration",
        "finished_at",
        "id",
        "mr_iid",
        "ref",
        "sha",
        "started_at",
        "username",
        "web_url",
    ],
    "merge_request": [
        "closed_at",
        "created_at",
        "id",
        "iid",
        "merged_at",
        "pipeline_id",
        "pipeline_finished_at",
        "pipeline_started_at",
        "pipeline_status",
        "pipeline_web_url",
        "title",
        "updated_at",
        "web_url",
        # "description",
    ],
    "mr_notes": [
        "author_name",
        "body",
        "created_at",
        "id",
        "mr_id",
        "mr_iid",
        "web_url",
        "updated_at",
    ],
    "sl": [
        "job_url",
        "pipeline_url",
        "fixed_tags",
    ],
    "sl_jobs": [
        "job_url",
        "job_name",
        "pipeline_url",
        "lava_job_id",
        "dut_name",
        "submitter_start_time",
        "submitter_end_time",
        "dut_submit_time",
        "dut_start_time",
        "dut_end_time",
        "dut_job_fail_reason",
    ],
    "sl_phases": [
        "job_url",
        "name",
        "start_time",
        "end_time",
        "dut_name",
    ],
    "utest": [
        "job_url",
        "unit_test",
        "used_duts",
    ],
    "failed_job": ["job_url", "full_name", "pipeline_id"],
}

TIME_KEY_CLASS: DBMeasurementsDict = {
    "mr_notes": "created_at",
    "merge_request": "updated_at",
    "job": "created_at",
    "pipeline": "created_at",
    "sl": "start_time",
    "sl_jobs": "submitter_start_time",
    "sl_phases": "start_time",
    "utest": "time",
    "failed_job": "finished_at",
}


def extract_point(entity_class: DB_MEASUREMENTS_KEYS, entity_obj: dict) -> Point:
    tag_keys = TAG_CLASS[entity_class]
    field_keys = FIELD_CLASS[entity_class]
    time_key = TIME_KEY_CLASS[entity_class]
    entity_obj["measurement"] = DB_MEASUREMENTS[entity_class]
    return Point.from_dict(
        entity_obj,
        record_time_key=time_key,
        record_tag_keys=tag_keys,
        record_field_keys=field_keys,
    )
