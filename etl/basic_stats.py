#!/usr/bin/env python

# Copyright (C) 2024 Collabora Ltd
# SPDX-License-Identifier: MIT


import asyncio
import argparse
import json as json
import logging
import re
from collections import defaultdict
from datetime import datetime, timezone
from functools import partial
from typing import Any, Callable

import aiohttp
from etl.extract.common import from_utest_to_db_points
from etl.extract.artifacts import (
    get_failed_unit_tests_deqp_failures_csv,
    get_failed_unit_tests_piglit_junit_xml,
)
from etl.extract.traces import categorize_failure_from_trace, get_failed_unit_tests_skqp
from gl_objects import DB_MEASUREMENTS, DB_MEASUREMENTS_KEYS, extract_point
from wrappers.influxdb_helper import InfluxdbSource, InfluxdbSourceAsync

from common.async_utils import (
    FetchParams,
    async_inspect,
    deferred_artifact_download,
    fetch_content,
    get_all_results,
    get_paginated_results,
)

INSPECT_INTERVAL_SEC: int = 10
# Number of resources to fetch per page in REST API
PER_PAGE: int = 100


def extract_mr_iid_from_ref(ref: str) -> int | None:
    ref_parts = ref.split("/")
    if len(ref_parts) == 1:
        return None
    if ref_parts[-2].isnumeric():
        return int(ref_parts[-2])
    return None


async def main(project_path: str = "mesa/mesa", until: str | None = None) -> None:
    logging.info(f"Fetching data for project {project_path}")
    db_source = InfluxdbSourceAsync("gitlab-stats")
    await db_source.ready()

    async with async_inspect(interval_sec=INSPECT_INTERVAL_SEC), db_source.client:
        since = await get_last_update(db_source, project_path)
        params = {}
        params["updated_after"] = since.astimezone(timezone.utc).isoformat()
        if until:
            params["updated_before"] = until
            if datetime.fromisoformat(until) < since:
                logging.fatal(f"Until date {until} is before the last update {since}")
                return
        # We need a non-standard timeout for the gitlab API due to the high ping times for some
        # places
        timeout = aiohttp.ClientTimeout(3 * 60, sock_read=2 * 60)
        async with aiohttp.ClientSession(timeout=timeout) as session:
            db_points: list[Any] = []
            fetch_params = FetchParams(project_path, session, db_source, db_points, params=params)
            crawler_tasks = [
                fetch_pipelines_and_jobs_to_db(fetch_params),
                fetch_mrs_to_db(fetch_params),
            ]
            await asyncio.gather(*crawler_tasks)
        # db_points are filled with the crawler_tasks that write to the db
        batching_idb_client = InfluxdbSource("gitlab-stats")
        with batching_idb_client.writer_instance():
            logging.info(f"Writing bulk of {len(db_points)} db points")
            for db_point in db_points:
                batching_idb_client.write(db_point)


async def get_job_artifacts(session, project_path: str, job_id: int, paths: list[str]):
    artifacts: list[bytes] = []
    for path in paths:
        artifact = None
        try:
            artifact = await deferred_artifact_download(session, project_path, job_id, path)
        except (aiohttp.ClientResponseError, aiohttp.ClientConnectionError) as e:
            logging.debug(f"Failed to fetch structured log for job {job_id}: {e}")
        except Exception as e:
            logging.error(f"Failed to fetch structured log for job {job_id}: {e}")
        if artifact:
            artifacts.append(artifact)
    return artifacts


def is_a_test_job(job: dict[str, Any]) -> bool:
    return not any(
        pattern in job.get("stage", "") for pattern in ("sanity", "build", "container", "deploy")
    )


async def extract_piglit_junit_xml(fp: FetchParams, job: dict[str, Any], used_duts: list[str]):
    junit_xml = await get_job_artifacts(
        fp.session,
        job["project_path"],
        job["id"],
        [
            "results/junit.xml",
        ],
    )
    if not junit_xml:
        return

    junit_xml = junit_xml[0].decode("utf-8")
    # deqp_runner failures
    junit_failures = get_failed_unit_tests_piglit_junit_xml(junit_xml)
    for point in from_utest_to_db_points(job, junit_failures, used_duts, "piglit"):
        fp.db_points.append(extract_point("utest", point))


async def extract_failures_csv(fp: FetchParams, job: dict[str, Any], used_duts: list[str]):
    failures_csv = await get_job_artifacts(
        fp.session,
        job["project_path"],
        job["id"],
        [
            "results/failures.csv",
        ],
    )
    if not failures_csv:
        return

    failures_csv = failures_csv[0].decode("utf-8")
    # deqp_runner failures
    dr_failures = get_failed_unit_tests_deqp_failures_csv(failures_csv)
    for point in from_utest_to_db_points(job, dr_failures, used_duts, "deqp"):
        fp.db_points.append(extract_point("utest", point))


async def extract_structured_log_points(fp: FetchParams, job: dict[str, Any]) -> list[str]:
    if not (job.get("tag_list") and is_a_test_job(job)):
        return []

    # get structured logs only for DUT and virtual jobs
    structured_log_file = await get_job_artifacts(
        fp.session,
        job["project_path"],
        job["id"],
        [
            "results/lava_job_detail.json",
            # TODO: Add support for other structured logs
            # "results/job_detail.json"
        ],
    )
    if not structured_log_file:
        return []

    duts_used_by_job: list[str] = []
    structured_log = json.loads(structured_log_file[0])
    dut_jobs = structured_log.pop("dut_jobs")
    dut_type = structured_log.get("dut_job_type", "Unknown")
    pipeline_url = job["web_url"].replace(
        f"/-/jobs/{job['id']}", f"/-/pipelines/{job['pipeline_id']}"
    )
    extract_structured_log(fp, job, structured_log, pipeline_url)

    if structured_log.get("job_combined_status") == "interrupted":
        return duts_used_by_job

    for attempt, dut_job in enumerate(dut_jobs, start=1):
        duts_used_by_job.append(dut_job["dut_name"])
        job_phases = dut_job.pop("dut_job_phases", {})
        extract_dut_job(fp, job, attempt, dut_job, dut_type, pipeline_url)
        extract_job_phases(fp, job, dut_job, job_phases)

    return duts_used_by_job


def extract_structured_log(fp: FetchParams, job: dict, structured_log: dict, pipeline_url: str):
    structured_log["job_url"] = job["web_url"]
    structured_log["pipeline_url"] = pipeline_url
    structured_log["start_time"] = job["started_at"]
    fp.db_points.append(extract_point("sl", structured_log))


def extract_dut_job(
    fp: FetchParams, job: dict, attempt: int, dut_job: dict, dut_type: str, pipeline_url: str
):
    dut_job["job_url"] = job["web_url"]
    dut_job["job_name"] = job["name"]
    dut_job["pipeline_url"] = pipeline_url
    dut_job["job_attempt"] = attempt
    dut_job["dut_job_type"] = dut_type
    fp.db_points.append(extract_point("sl_jobs", dut_job))


def extract_job_phases(fp: FetchParams, job: dict, dut_job: dict, job_phases: dict):
    for name, phase in job_phases.items():
        phase = {
            "job_url": job["web_url"],
            "job_attempt": dut_job["job_attempt"],
            "name": name,
            "start_time": phase["start_time"],
            "end_time": phase["end_time"],
            "dut_name": dut_job["dut_name"],
        }
        fp.db_points.append(extract_point("sl_phases", phase))


async def post_process_job(fp: FetchParams, job: dict[str, Any], session: aiohttp.ClientSession):
    if not job.get("artifacts"):
        return

    if not job.get("username") == "marge-bot":
        # This is a costly operation, so we only do it for marge-bot jobs
        return

    used_duts = await extract_structured_log_points(fp, job)

    if job.get("status") != "failed":
        return

    if job.get("finished_at"):
        # only finished and failed jobs produce non-empty failures.csv
        await extract_failures_csv(fp, job, used_duts)

        # NOTE: this should be covered by the deqp-runner csv
        # Uncomment if we have some piglit job not wrapped by deqp-runner
        # if "piglit" in job["name"]:
        #     await extract_piglit_junit_xml(fp, job, used_duts)

    await process_gitlab_log(fp, job, session)
    await process_raw_trace(fp, job, session, used_duts)


async def process_gitlab_log(fp, job, session):
    # get the trace for failed jobs
    try:
        trace = await fetch_content(job["web_url"] + "/trace", session)
    except (aiohttp.ClientError, aiohttp.ClientOSError) as e:
        logging.error(f"Failed to fetch trace for job {job['id']}: {e}")
        return
    else:
        if not trace:
            logging.debug(f"Empty trace for job {job['id']}")
            return


async def process_raw_trace(fp, job, session, used_duts):
    try:
        raw_trace = await fetch_content(job["web_url"] + "/raw", session)
    except (aiohttp.ClientError, aiohttp.ClientOSError) as e:
        logging.error(f"Failed to fetch raw trace for job {job['id']}: {e}")
        return
    else:
        if not raw_trace:
            logging.debug(f"Got empty trace from job {job['id']}")
            return
    raw_trace = raw_trace.decode("utf-8")
    skqp = get_failed_unit_tests_skqp(raw_trace)
    for point in from_utest_to_db_points(job, skqp, used_duts, "skqp"):
        fp.db_points.append(extract_point("utest", point))

    if "-performance" in job.get("name") or "-restricted" in job.get("name"):
        # Skip classifying performance/restricted jobs; they are allowed to fail and don't block
        # merges
        return

    fp.db_points.append(extract_point("failed_job", categorize_failure_from_trace(job, raw_trace)))


async def fetch_jobs_to_db(fp: FetchParams, pipeline_id: int) -> None:
    jobs: list[dict[str, Any]] = []
    traces_tasks: list[asyncio.Task[None]] = []
    async for jobs_json in get_paginated_results(
        fp,
        f"pipelines/{pipeline_id}/jobs",
        include_retried="true",
        per_page=PER_PAGE,
    ):
        for job in jobs_json:
            name_match = re.match(r"\S+", job["name"])
            if not name_match:
                continue
            job |= {
                "full_name": job["name"],
                "name": name_match[0],
                "pipeline_id": int(job["pipeline"]["id"]),
                "sha": job["commit"]["id"],
                "tag_list": sorted(job["tag_list"]),
                "username": job["user"]["username"],
                "project_path": fp.project_path,
            }
            traces_tasks.append(asyncio.create_task(post_process_job(fp, job, fp.session)))
            jobs.append(job)

    if not jobs:
        logging.info(f"No jobs found for pipeline {pipeline_id}")
        return

    # assuming greater ids mean newer jobs
    jobs = sorted(jobs, key=lambda x: (x["name"], x["id"]))
    extractor = partial(extract_point, "job")
    # iterate in pairs, but put the last job first, as the next iteration will miss it
    points = [extractor(jobs[-1])]
    for job, next_job in zip(jobs, jobs[1:]):
        job["retried"] = job["name"] == next_job["name"]
        points.append(extractor(job))

    await asyncio.gather(*traces_tasks)
    fp.db_points.extend(points)


async def fetch_pipelines_and_jobs_to_db(fp: FetchParams) -> None:
    logging.info("Fetching Pipelines and jobs")
    # change a datetime to utc
    jobs_tasks: list[asyncio.Task[None]] = []
    full_pipeline_tasks: list[asyncio.Task[None]] = []

    def extra_fields_fn(pipeline_json: dict[str, Any]) -> dict:
        return {
            "mr_iid": extract_mr_iid_from_ref(pipeline_json["ref"]),
            "username": pipeline_json["user"]["username"],
        }

    async for pipe_json in get_paginated_results(fp, "pipelines"):
        for pipe in pipe_json:
            pipe |= {
                "project_path": fp.project_path,
            }
            full_pipeline_tasks.append(
                asyncio.create_task(get_full_resource(fp, "pipeline", pipe, extra_fields_fn))
            )
            jobs_tasks.append(asyncio.create_task(fetch_jobs_to_db(fp, pipeline_id=pipe["id"])))
    await asyncio.gather(*(full_pipeline_tasks + jobs_tasks))
    logging.info("Done Pipelines and jobs")


async def fetch_mrs_to_db(fp: FetchParams):
    notes_tasks: list[asyncio.Task[None]] = []
    full_mr_tasks: list[asyncio.Task[None]] = []
    logging.info("Fetching MRs")

    def extra_fields_fn(mr_json: dict[str, Any]) -> dict:
        basic_extra_fields: dict[str, str] = {
            "project_path": fp.project_path,
        }
        if not mr_json.get("head_pipeline"):
            return basic_extra_fields

        mr_pipeline = defaultdict(lambda: None, mr_json["head_pipeline"])
        return {
            **basic_extra_fields,
            "pipeline_id": mr_pipeline["id"],
            "pipeline_finished_at": mr_pipeline["finished_at"],
            "pipeline_started_at": mr_pipeline["started_at"],
            "pipeline_status": mr_pipeline["status"],
            "pipeline_web_url": mr_pipeline["web_url"],
        }

    async for mr_json in get_paginated_results(
        fp,
        "merge_requests",
        per_page=PER_PAGE,
    ):
        for mr in mr_json:
            full_mr_tasks.append(
                asyncio.create_task(
                    get_full_resource(fp, "merge_request", mr, extra_fields_fn, "iid")
                )
            )
            notes_tasks.append(asyncio.create_task(fetch_mrs_notes(fp, mr)))

    await asyncio.gather(*(notes_tasks + full_mr_tasks))
    logging.info("Done MRs")


async def fetch_mrs_notes(fp: FetchParams, mr: dict[str, Any]):
    async for notes_json in get_paginated_results(
        fp, f"merge_requests/{mr['iid']}/notes", per_page=PER_PAGE
    ):
        mr_notes_points = []
        for mr_notes in notes_json:
            mr_notes |= {
                "mr_id": mr["id"],
                "mr_iid": mr["iid"],
                "mr_updated_at": mr["updated_at"],
                "author_name": mr_notes["author"]["username"],
                "project_path": fp.project_path,
            }
            mr_notes_points.append(extract_point("mr_notes", mr_notes))

        fp.db_points.extend(mr_notes_points)


async def get_full_resource(
    fp: FetchParams,
    resource: DB_MEASUREMENTS_KEYS,
    partial_json: dict[str, Any],
    extra_fields_fn: Callable[[dict[str, Any]], dict[str, Any]] = lambda x: {},
    id_field: str = "id",
) -> None:
    """
    Fetches the full pipeline json and writes it to the db.
    It has some useful data such as the MR iid and the username of the user that created it.
    """
    if full_json := await get_all_results(
        fp,
        f"{resource}s/{partial_json[id_field]}",
    ):
        partial_json = full_json
    partial_json |= extra_fields_fn(partial_json)
    point = extract_point(resource, partial_json)
    fp.db_points.append(point)


async def get_last_update(db_source: InfluxdbSourceAsync, project_path: str) -> datetime:
    assert isinstance(DB_MEASUREMENTS["merge_request"], str)
    since = await db_source.async_get_last_write(
        measurement=DB_MEASUREMENTS["merge_request"],
        field="id",
        range_start="-30d",
        tags_match={"project_path": project_path},
    )

    logging.info(f"Last update for {project_path} was {since}")

    return since


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-pp", "--project-path", default="mesa/mesa", help="Project path to fetch data"
    )
    parser.add_argument(
        "-u", "--until", default=None, help="Fetch UTC data until this date (ISO 8601)"
    )
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    logging.basicConfig(
        level=logging.INFO,
        format=f"%(asctime)s - %(name)s - %(levelname)s - [{args.project_path}] %(message)s",
    )
    # Switch to debug=True to see debug messages
    # asyncio.run(main(), debug=True)
    asyncio.run(main(args.project_path, args.until))
