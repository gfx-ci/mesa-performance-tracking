from xml.etree import ElementTree as ET


def get_failed_unit_tests_deqp_failures_csv(failures_csv: str) -> dict[str, str]:
    if not failures_csv:
        return {}

    failed_utests = {}
    lines = failures_csv.splitlines()
    for line in lines:
        split = line.split(",")
        reason = split[-1]
        if test_name := split[0]:
            failed_utests[test_name] = reason
    return failed_utests


def get_failed_unit_tests_piglit_junit_xml(junit_xml: str) -> dict[str, str]:
    if not junit_xml:
        return {}

    root = ET.fromstring(junit_xml)
    failed_tests: dict[str, str] = {}

    for testcase in root.iter("testcase"):
        reason = ""
        if testcase.get("status") == "fail":
            reason = str(testcase.get("status"))
        else:
            failure = testcase.find("failure")
            if failure is None:
                continue
            reason = str(failure.attrib["type"])
        if not reason:
            continue
        name = testcase.get("name", "None")
        failed_tests[name] = reason

    return failed_tests
