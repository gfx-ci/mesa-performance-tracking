from functools import cache
from typing import Any
import yaml

CONFIG_TRACES_FILE_PATH: str = "./mesa-ci-status/traces_config.yaml"


def from_utest_to_db_points(
    job: dict[str, Any], failed_utests: dict[str, str], used_duts: list[str], test_suite: str = ""
):
    for utest, reason in failed_utests.items():
        # duts = used_duts  # or "-".join(job.get("tag_list", []))
        point = {
            "time": job["finished_at"],
            "job_name": job.get("name"),
            "stage": job.get("stage"),
            "reason": reason.lower(),
            "job_url": job.get("web_url"),
            "unit_test": utest,
            "used_duts": ", ".join(used_duts),
            "test_suite": test_suite,
        }
        yield point


@cache
def traces_config() -> dict[str, Any]:
    with open(CONFIG_TRACES_FILE_PATH) as f:
        return yaml.load(f, Loader=yaml.FullLoader)
