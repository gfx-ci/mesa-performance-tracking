from functools import lru_cache
import re
from typing import Any
from etl.extract.common import traces_config


def get_failed_unit_tests_skqp(trace: str) -> dict[str, str]:
    if not trace:
        return {}

    # Get test names, examples:
    # [0m2022-05-24 13:56:28.106391: Starting: gl_zero_control_stroke
    # [0m2022-05-24 13:58:35.054641: Starting test: unitTest_AbandonedContextImage  # noqa: E501
    pattern = r": Starting.*: *(.+)"
    tests = re.findall(pattern, trace)
    if not tests:
        return {}

    # Get test status, examples:
    # [0m2022-05-24 13:56:07.475938: Passed:   gl_pathopsinverse
    # [0m2022-05-24 13:55:31.365298: FAILED:   gl_atlastext (19)
    # [0m2022-05-24 13:58:35.054677: Test passed:   unitTest_AbandonedContextImage  # noqa: E501
    # Get status from lines with the test name that doesn't have
    # "Starting" word
    tests_p = "|".join(tests)
    pattern = f": ((?!Starting).*): *({tests_p})"
    res = re.findall(pattern, trace)

    return {test: status for status, test in res if "passed" not in status.lower()}


@lru_cache(1)
def load_and_precompile_patterns() -> list[dict[str, Any]]:
    categories = traces_config()["categories"]
    for category in categories:
        # Compile each pattern individually
        compiled_patterns = [re.compile(p) for p in category["patterns"]]
        category["compiled_patterns"] = compiled_patterns
    return categories


def categorize_errors(
    trace: str, chunk_size_chars: int = 10000, buffer_size_chars: int = 100
) -> dict[str, Any]:
    """
    Categorizes errors in a given trace by searching for patterns in chunks.

    Knowing that the end of the trace is more likely to contain relevant error,
    this function processes the trace string in reverse, dividing it into chunks
    of a specified size. It searches for precompiled regex patterns within each
    chunk, including a buffer overlap between chunks to handle patterns that may
    span across chunk boundaries.

    Parameters:
        trace (str): The trace string containing error logs or messages.
        chunk_size_chars (int, optional): The number of characters to include in
            each chunk. Defaults to 10,000 characters.
        buffer_size_chars (int, optional): The number of characters to overlap
            between chunks to handle patterns spanning chunks. Defaults to 100
            characters.

    Returns:
        dict[str, Any]: A dictionary representing the category of the error found.
            If no known category is found, it returns the 'unknown' category.

    Notes:
        - The function relies on `load_and_precompile_patterns()` to retrieve
            categories and their compiled regex patterns.
        - It iteratively searches from the end of the trace to the beginning to
            improve performance, especially when relevant patterns are likely to be
            found near the end.
        - Categories with a 'tag' of "unknown" are skipped during the search and
            are only returned if no other category matches.

    Example:
        >>> trace_log = "Error: Connection timed out after 30 seconds..."
        >>> category = categorize_errors(trace_log)
        >>> print(category['name'])
        'TimeoutError'
    """
    trace = trace or ""
    total_length = len(trace)
    start_index = total_length

    while start_index > 0:
        end_index = start_index
        start_index = max(0, start_index - chunk_size_chars)

        # Extract the chunk
        chunk = trace[start_index:end_index]

        # Include buffer from next chunk, if any
        buffer_end = min(total_length, end_index + buffer_size_chars)
        buffer = trace[end_index:buffer_end]

        # Combine chunk and buffer
        chunk_with_buffer = chunk + buffer

        # Search for known patterns
        for category in load_and_precompile_patterns():
            for pattern in category["compiled_patterns"]:
                if pattern.search(chunk_with_buffer) and category["tag"] != "unknown":
                    return category

    # Return unknown category
    return load_and_precompile_patterns()[-1]


def categorize_failure_from_trace(job: dict, trace: str) -> dict[str, str]:
    category = categorize_errors(trace)
    return {
        "finished_at": job["finished_at"],
        "job_name": job.get("name", ""),
        "full_name": job.get("full_name", ""),
        "job_url": job.get("web_url", ""),
        "stage": job.get("stage", ""),
        "pipeline_id": job.get("pipeline_id", ""),
        "category": category["name"],
        "tag": category["tag"],
    }
