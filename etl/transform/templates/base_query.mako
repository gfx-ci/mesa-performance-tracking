import "influxdata/influxdb/schema"

from(bucket: "${bucket}")
    |> range(start: ${range})
    |> filter(fn: (r) => r._measurement == "${measurement}")
% if early_field_cols:
    |> schema.fieldsAsCols()
% endif
% if filters:
    % for filter in filters:
    |> filter(fn: (r) => ${filter})
    % endfor
% endif
% if late_field_cols:
    |> schema.fieldsAsCols()
% endif
% if drop:
    |> drop(columns: ["_start", "_stop", "_measurement"])
% endif
% if group:
    |> group()
% endif
